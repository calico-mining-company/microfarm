# microfarm project

We are building an automated microfarm!

Currently we have a soil monitoring device connected to an Arduino to a Linux device (for us, Raspberry Pi 4) and the code for the Arduino to read the soil monitor.

Check out each main directory for more README action as well.

## Coming Soon

+ Docker container to update the code on the Arduino
+ Docker container to read the logs from the monitoring container (likely ELK stack)
+ Docker Compose to launch all these containers at once
+ Additional sensors to monitor plant's environment
+ Docker container taking timelapse photos & saving them in a volume
+ Parts list to reproduce this at home

## To Do

## Functionality

Everything in one Docker Compose file; deploying any code changes is just redeploying the Docker Compose probably
