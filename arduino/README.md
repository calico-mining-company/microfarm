# Source Code for Arduino

Here is the source code the Arduino will run, and coming soon, a Docker container that will deploy the source code to the Arduino.

## Source Code

Currently only reads soil moisture sensor for one device and engages the pump to water the plant when the sensor reads above 550 (the higher the number, the higher the resistence, meaning less water or drier soil). 

To Do:
+ Change source code to only engage one pump at a given time (for power consumption requirements)
+ Add additional code for more soil moisture sensors and pumps
+ Add additional code for additional sensors

## Dockerfile

To Do:
+ Deploy Arduino IDE using Linux CLI first, then dockerize that
+ Build Dockerfile that reads code on Arduino, checks for differences, and then ieploys any code changes
+ Integrate CI/CD for source code changes
