# Monitoring any Arduino Serial Device with Docker/Python

Connect any serial device to an Arduino. Connect the Arduino via USB to Linux. Build container + run (see bash_history.txt for commands).

## How To Use

### 1. Find where the Arduino is located in /dev

Assuming the Arduino is connected via USB (in our case, to a RPi4), it will show up with `lsusb` and also in `/dev` (assuming the drivers are present; check this with `lsusb -t -v` under "Driver"). Use `ls /dev/tty* | grep USB`. Note the device may be present as `ttyAMC`, in which case use `grep AMC`. By default, the device will be `/dev/ttyUSB0`.

### 2. Build the Docker container

Now, change directories to where the Dockerfile is located (the one in this directory in the repository. Use the following command:

`docker build -t monitor-arduino:0.1 .`

You can change the `monitor-arduino` and `0.1` to whatever you'd like.

You can check the images you have built with `docker images`.

### 3. Run the Docker container

Now run the container in the background.

`docker run -dit --device="/dev/ttyUSB0" monitor-arduino:0.1`

Change the device path and the image name to the proper values if your system named the USB device differently or if you named the image differently.

Docker will output the container name (a long hash). This is ephemeral, but you will need to know the first 3-4 characters for the next section. You can also check this with `docker ps -a`.

### 4. View Docker logs

You can view the last 20 Docker logs of the container like this:

`docker logs -n 20 ***` 
where `***` is the first few characters of the container ID.

You can also view the logs live with:

`docker attach ***`.

Congrats! You should have a working container. Otherwise, it's learning time, find those errors and fix them!
